package com.game.tictactoe.Entity;


import com.game.tictactoe.Player;

/**
 * Created by sgandhi on 7/27/17.
 */
public class Cell {

    public int row;
    public int col;
    public Player player;

    public Cell(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Cell(int row, int col, Player player) {
        this.row = row;
        this.col = col;
        this.player = player;
    }

}
