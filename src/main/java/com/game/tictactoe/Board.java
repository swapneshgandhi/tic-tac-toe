package com.game.tictactoe;

import com.game.tictactoe.Entity.Cell;

import java.util.Arrays;

/**
 * Created by sgandhi on 7/27/17.
 */
public class Board {

    Cell[][] board;


    public Board() {
        board = new Cell[3][3];

        for (Cell[] singleRow : board) {
            Arrays.fill(singleRow, null);
        }
    }

    public void addToken(Cell cell) {

        board[cell.row][cell.col] = cell;

    }

    public void printBoard() {

        for (int i = 0; i < board.length; i++) {

            for (int j = 0; j < board[0].length; j++) {

                if (board[i][j] == null || board[i][j].player == null) {
                    if (j == board[0].length - 1) {
                        System.out.print("-");
                    } else {
                        System.out.print("-" + "|");
                    }

                } else {

                    if (j == board[0].length - 1) {
                        System.out.print(board[i][j].player.symbol);
                    } else {
                        System.out.print(board[i][j].player.symbol + "|");
                    }

                }

            }
            System.out.println();
        }

    }

    public boolean isOccupied(int row, int col) {
        return board[row][col] != null;
    }

    public boolean isFull() {

        for (int i = 0; i < board.length; i++) {

            for (int j = 0; j < board[0].length; j++) {

                if (!isOccupied(i, j)) {
                    return false;
                }

            }


        }
        return true;
    }
}


