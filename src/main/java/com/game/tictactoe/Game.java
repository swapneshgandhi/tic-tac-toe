package com.game.tictactoe;

import com.game.tictactoe.Entity.Cell;

/**
 * Created by sgandhi on 7/27/17.
 */
public class Game {

    public static final String UserSym = "X";
    public static final String CompSym = "O";


    public static void main(String[] args) throws Exception {

        Board board = new Board();

        Cell cell1 = new Cell(0, 1, new User(UserSym));
        board.addToken(cell1);
        board.printBoard();
        //board.isFull();

        Computer comp = new Computer(CompSym);

        board.addToken(comp.play(board));
        board.printBoard();
        board.addToken(comp.play(board));
        board.printBoard();
    }

}
