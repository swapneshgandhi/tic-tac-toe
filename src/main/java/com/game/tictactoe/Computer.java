package com.game.tictactoe;

import com.game.tictactoe.Entity.Cell;

/**
 * Created by sgandhi on 7/27/17.
 */
public class Computer extends Player {


    public Computer(String symbol) {
        this.symbol = symbol;
    }

    public Cell play(Board board) throws Exception {


        for (int i = 0; i < board.board.length; i++) {

            for (int j = 0; j < board.board[0].length; j++) {

                if (!board.isOccupied(i, j)) {
                    return new Cell(i, j, new Computer(Game.CompSym));
                }

            }


        }
        throw new Exception("Board is full");
    }


}
